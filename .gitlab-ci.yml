image: alpine:latest

variables:
  # KUBE_INGRESS_BASE_DOMAIN is the application deployment domain and should be set as a variable at the group or project level.
  # KUBE_INGRESS_BASE_DOMAIN: domain.example.com

  POSTGRES_ENABLED: "false"
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  ROLLOUT_RESOURCE_TYPE: deployment
  INCREMENTAL_ROLLOUT_MODE: manual

stages:
  - build
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%

build:
  stage: build
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image:v0.4.0"
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:19.03.12-dind
  script:
    - |
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
    - /build/build.sh
  rules:
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'

.auto-deploy:
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v1.0.7"
  dependencies: []

.production: &production_template
  extends: .auto-deploy
  stage: production
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy
    - auto-deploy delete rollout
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths: [environment_url.txt, tiller.log]
    when: always

production:
  <<: *production_template
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'

production_manual:
  <<: *production_template
  allow_failure: false
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never

# This job implements incremental rollout on for every push to `master`.

.rollout: &rollout_template
  extends: .auto-deploy
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy rollout $ROLLOUT_PERCENTAGE
    - auto-deploy scale stable $((100-ROLLOUT_PERCENTAGE))
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths: [environment_url.txt, tiller.log]
    when: always

.manual_rollout_template: &manual_rollout_template
  <<: *rollout_template
  stage: production
  resource_group: production
  allow_failure: true
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "timed"'
      when: never
    - if: '$CI_COMMIT_BRANCH != "master"'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "manual"'
      when: manual

.timed_rollout_template: &timed_rollout_template
  <<: *rollout_template
  rules:
    - if: '$CI_KUBERNETES_ACTIVE == null || $CI_KUBERNETES_ACTIVE == ""'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "manual"'
      when: never
    - if: '$CI_COMMIT_BRANCH != "master"'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE == "timed"'
      when: delayed
      start_in: 1 minutes

timed rollout 10%:
  <<: *timed_rollout_template
  stage: incremental rollout 10%
  variables:
    ROLLOUT_PERCENTAGE: 10

timed rollout 25%:
  <<: *timed_rollout_template
  stage: incremental rollout 25%
  variables:
    ROLLOUT_PERCENTAGE: 25

timed rollout 50%:
  <<: *timed_rollout_template
  stage: incremental rollout 50%
  variables:
    ROLLOUT_PERCENTAGE: 50

timed rollout 100%:
  <<: *timed_rollout_template
  <<: *production_template
  stage: incremental rollout 100%
  variables:
    ROLLOUT_PERCENTAGE: 100

rollout 10%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 10

rollout 25%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 25

rollout 50%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 50

rollout 100%:
  <<: *manual_rollout_template
  <<: *production_template
  allow_failure: false
